package com.zongtui.fourinone.SayHello;

import com.zongtui.fourinone.Contractor;
import com.zongtui.fourinone.WareHouse;
import com.zongtui.fourinone.WorkerLocal;

/**
 * ClassName: HelloCtor <br/>
 * Function: 工头实现. <br/>
 * date: 2015-4-10 上午10:58:06 <br/>
 *
 * @author feng
 * @version 
 * @since JDK 1.7
 */
public class HelloCtor extends Contractor
{
	public WareHouse giveTask(WareHouse inhouse)
	{
		WorkerLocal[] wks = getWaitingWorkers("helloworker");
		System.out.println("wks.length:"+wks.length);
		WareHouse wh = new WareHouse("word", "hello, i am your Contractor.");
		WareHouse[] hmarr = doTaskBatch(wks, wh);

		for(WareHouse result:hmarr)
			System.out.println(result);

		return null;
	}
	
	public static void main(String[] args)
	{
		HelloCtor a = new HelloCtor();
		a.giveTask(null);
		a.exit();
	}
}