package com.zongtui.fourinone.SayHello;

import com.zongtui.fourinone.MigrantWorker;
import com.zongtui.fourinone.WareHouse;
import com.zongtui.fourinone.Workman;

/**
 * ClassName: HelloWorker <br/>
 * Function: 工人实现. <br/>
 * date: 2015-4-10 上午10:57:14 <br/>
 *
 * @author feng
 * @version 
 * @since JDK 1.7
 */
public class HelloWorker extends MigrantWorker
{
	/**
	 * serialVersionUID.
	 */
	private static final long serialVersionUID = 1L;
	private String name;
	public HelloWorker(String name){
		this.name = name;
	}

	public WareHouse doTask(WareHouse inhouse)
	{
		System.out.println(inhouse.getString("word"));
		WareHouse wh = new WareHouse("word", "hello, i am "+name);
		Workman[] wms = getWorkerElse("helloworker");
		for(Workman wm:wms)
			wm.receive(wh);
		return wh;
	}
	
	public boolean receive(WareHouse inhouse)
	{
		System.out.println(inhouse.getString("word"));
		return true;
	}
	
	public static void main(String[] args)
	{
		HelloWorker mw = new HelloWorker(args[0]);
		mw.waitWorking(args[1],Integer.parseInt(args[2]),"helloworker");
	}
}