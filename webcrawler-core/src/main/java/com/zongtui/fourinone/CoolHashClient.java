package com.zongtui.fourinone;

public interface CoolHashClient extends CoolHash{
	public void begin();
	public void rollback();
	public void commit();
	public void exit();
}