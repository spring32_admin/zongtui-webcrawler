package com.zongtui.fourinone;

import com.zongtui.fourinone.FileAdapter.ByteReadParser;
import com.zongtui.fourinone.FileAdapter.ByteWriteParser;

interface CoolHashBase{
	CoolHashException chex = new CoolHashException();
	ByteWriteParser bwp = DumpAdapter.getByteWriteParser();
	ByteReadParser brp = DumpAdapter.getByteReadParser();
	ConstantBit.Target ct = ConstantBit.Target.POINT;
	DumpAdapter dumpAdapter = new DumpAdapter("");
}