/**
 * Project Name:webcrawler-sourceer
 * File Name:SourceBuilder.java
 * Package Name:com.zongtui.webcrawler
 * Date:2015年4月11日下午11:45:26
 * Copyright (c) 2015, 众推项目组版权所有.
 *
 */
package com.zongtui.webcrawler;

/**
 * ClassName: SourceBuilder <br/>
 * Function: 数据产品生成接口. <br/>
 * date: 2015年4月11日 下午11:45:26 <br/>
 *
 * @author zhangfeng
 * @version 
 * @since JDK 1.7
 */
public interface ISourceBuilder {
	
	/**
	 * buildSourceByCrawler:由爬虫构造数据. <br/>
	 *
	 * @author zhangfeng
	 * @since JDK 1.7
	 */
	public void buildSourceByCrawler();
	
	/**
	 * buildSourceByFile:由文件构造数据. <br/>
	 *
	 * @author zhangfeng
	 * @since JDK 1.7
	 */
	public void buildSourceByFile();
	
	/**
	 * buildSourceByFtp:由ftp构造数据. <br/>
	 *
	 * @author zhangfeng
	 * @since JDK 1.7
	 */
	public void buildSourceByFtp();
	
	/**
	 * retrieveResult:得到结果. <br/>
	 *
	 * @author zhangfeng
	 * @return
	 * @since JDK 1.7
	 */
	public IData retrieveResult();

}
