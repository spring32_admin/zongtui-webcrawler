package com.zongtui.webcrawler.sourceer.downloader;

import com.zongtui.webcrawler.sourceer.Page;
import com.zongtui.webcrawler.sourceer.Request;
import com.zongtui.webcrawler.sourceer.Site;
import com.zongtui.webcrawler.sourceer.selector.Html;

/**
 * ClassName: AbstractDownloader <br/>
 * Function: 下载模板类. <br/>
 * date: 2015年4月12日 下午1:23:09 <br/>
 *
 * @author code4crafter@gmail.com <br>
 * @version
 * @since JDK 1.7
 */
public abstract class AbstractDownloader implements Downloader {

	/**
	 * A simple method to download a url.
	 *
	 * @param url
	 * @return html
	 */
	public Html download(String url) {
		return download(url, null);
	}

	/**
	 * A simple method to download a url.
	 *
	 * @param url
	 * @return html
	 */
	public Html download(String url, String charset) {
		Page page = download(new Request(url), Site.me().setCharset(charset)
				.toTask());
		return (Html) page.getHtml();
	}

	protected void onSuccess(Request request) {
	}

	protected void onError(Request request) {
	}

	/**
	 * addToCycleRetry:加入循环重试. <br/>
	 *
	 * @author zhangfeng
	 * @param request
	 * @param site
	 * @return
	 * @since JDK 1.7
	 */
	protected Page addToCycleRetry(Request request, Site site) {
		Page page = new Page();
		Object cycleTriedTimesObject = request
				.getExtra(Request.CYCLE_TRIED_TIMES);
		if (cycleTriedTimesObject == null) {
			page.addTargetRequest(request.setPriority(0).putExtra(
					Request.CYCLE_TRIED_TIMES, 1));
		} else {
			int cycleTriedTimes = (Integer) cycleTriedTimesObject;
			cycleTriedTimes++;
			if (cycleTriedTimes >= site.getCycleRetryTimes()) {
				return null;
			}
			page.addTargetRequest(request.setPriority(0).putExtra(
					Request.CYCLE_TRIED_TIMES, cycleTriedTimes));
		}
		page.setNeedCycleRetry(true);
		return page;
	}
}
