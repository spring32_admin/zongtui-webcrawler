/**
 * Project Name:webcrawler-sourceer
 * File Name:SimpleBuildByCrawler.java
 * Package Name:com.zongtui.webcrawler
 * Date:2015年4月12日上午12:11:56
 * Copyright (c) 2015, 众推项目组版权所有.
 *
 */
package com.zongtui.webcrawler;

/**
 * ClassName: SimpleBuildByCrawler <br/>
 * Function: 简单爬虫测试类，输出爬虫返回的数据. <br/>
 * date: 2015年4月12日 上午12:11:56 <br/>
 *
 * @author zhangfeng
 * @version 
 * @since JDK 1.7
 */
public class SimpleBuildByCrawler extends SourceDirector {

	public SimpleBuildByCrawler(ISourceBuilder sourceBuilder) {
		super(sourceBuilder);
	}
	
	public static void main(String[] args) {
		ISourceBuilder crawlerBuilder = new CrawlerDataBuilder();
		SourceDirector sourceDirector = new SimpleBuildByCrawler(crawlerBuilder);
		sourceDirector.construct();
		IData crawlerData = crawlerBuilder.retrieveResult();
		System.out.println(crawlerData);
	}

}
